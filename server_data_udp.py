import asyncio
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

messageServer = None
okServer = ""


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        if data.decode() == "OK_Server":
            print("Connection established")
            global okServer
            okServer = data.decode()
        elif "sdp" and "offer" in data.decode():
            global messageServer
            messageServer = data.decode()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def waitRemoteDescription(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)


async def consume_signaling(pc):
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()

    global messageServer
    messageServer = "REGISTER SERVER"

    echoConnection = EchoClientProtocol(messageServer, on_con_lost)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: echoConnection, remote_addr=('127.0.0.1', 9999))

    await asyncio.sleep(5)

    if "sdp" and "offer" in messageServer:
        offerDict = json.loads(messageServer)
        offerSDP = offerDict['sdp']
        offerSDPType = offerDict['type']
        offerAnswer = RTCSessionDescription(offerSDP, offerSDPType)
        if isinstance(offerAnswer, RTCSessionDescription):
            await pc.setRemoteDescription(offerAnswer)

        await pc.setLocalDescription(await pc.createAnswer())
        answer = pc.localDescription
        messageServer = json.dumps(answer.__dict__)
        echoConnection.transport.sendto(messageServer.encode())
        print("Send:", messageServer)
        await asyncio.sleep(5)
        await waitRemoteDescription(pc)



time_start = None


async def run_answer(pc):
    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")
        print(f"Communication channel opened -> {channel.label}")

        @channel.on("message")
        def on_message(message):
            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    await consume_signaling(pc)


if __name__ == "__main__":
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    pc = RTCPeerConnection()
    coro = run_answer(pc)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
