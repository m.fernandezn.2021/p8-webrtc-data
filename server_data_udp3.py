import asyncio
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

messageServer = None
okServer = ""
echoConnection = None


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        global messageServer
        if data.decode() == "OK_Server":
            print("Connection established")
            global okServer
            okServer = data.decode()
        if "sdp" and "offer" in data.decode():
            messageServer = data.decode()
        else:
            messageServer = data.decode()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")


async def waitRemoteDescription(pc):
    while pc.remoteDescription is None:
        await asyncio.sleep(1)


def resetVariables():
    global messageServer, okServer
    messageServer = None
    okServer = ""


async def consume_signaling(pc, echo):
    global messageServer
    if echo is None:
        loop = asyncio.get_running_loop()
        on_con_lost = loop.create_future()
        messageServer = "REGISTER SERVER"
        echo = EchoClientProtocol(messageServer, on_con_lost)

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    messageServer = "REGISTER SERVER"
    echo = EchoClientProtocol(messageServer, on_con_lost)
    await loop.create_datagram_endpoint(lambda: echo, remote_addr=('127.0.0.1', 9999))

    while True:
        await asyncio.sleep(5)
        if "sdp" and "offer" in messageServer:
            offerDict = json.loads(messageServer)
            offerSDP = offerDict['sdp']
            offerSDPType = offerDict['type']
            offerAnswer = RTCSessionDescription(offerSDP, offerSDPType)
            if isinstance(offerAnswer, RTCSessionDescription):
                await pc.setRemoteDescription(offerAnswer)

            await pc.setLocalDescription(await pc.createAnswer())
            answer = pc.localDescription
            messageServer = json.dumps(answer.__dict__)
            echo.transport.sendto(messageServer.encode())
            print("Send:", messageServer)
            await asyncio.sleep(2)
            await waitRemoteDescription(pc)

        if "bye" in messageServer: # Bye message and reset of variables
            resetVariables()
            pc = RTCPeerConnection()
            await run_answer(pc)


time_start = None


async def run_answer(pc):
    global echoConnection
    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")
        print(f"Communication channel opened -> {channel.label}")

        @channel.on("message")
        def on_message(message):
            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)
    await consume_signaling(pc, echoConnection)


if __name__ == "__main__":
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    pc = RTCPeerConnection()
    coro = run_answer(pc)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
