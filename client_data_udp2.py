import asyncio
import time
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription, sdp
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        # self.counter = 0

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        #self.counter += 1
        #byeMessage = {"type": "bye"}
        global confirmedConnection
        confirmedConnection = data.decode()
        #if self.counter == 3:
        #    print("Sending BYE to server")
        #    self.transport.sendto(json.dumps(byeMessage).encode())
        #if "bye Client" in confirmedConnection:
        #    print("Gotbye:", confirmedConnection, "\n Closing connection...")
        #    self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)
        quit(0)


confirmedConnection = None
messageClient = None
echoConnectionClient = None


async def waitRemoteDescription(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)


async def waitForMessage():
    while confirmedConnection == "OK_Client":
        await asyncio.sleep(1)


async def consume_signaling(pc): # Chasing Info provided in case it's available
    # for a session to create an offer, taking into account BYE messages
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()

    global messageClient, echoConnectionClient
    messageClient = "REGISTER CLIENT"
    echoConnectionClient = EchoClientProtocol(messageClient, on_con_lost)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: echoConnectionClient, remote_addr=('127.0.0.1', 9999))
    await asyncio.sleep(1)
    global confirmedConnection
    if confirmedConnection == "OK_Client":
        obj = pc.localDescription
        messageClient = json.dumps(obj.__dict__)
        await asyncio.sleep(1)
        echoConnectionClient.transport.sendto(messageClient.encode())
        print("Send:", messageClient)
        await waitForMessage()

    if "sdp" and "answer" in confirmedConnection:
        await asyncio.sleep(2)
        answerDict = json.loads(confirmedConnection)
        answerSDP = answerDict['sdp']
        answerSDPType = answerDict['type']
        objAnswer = RTCSessionDescription(answerSDP, answerSDPType)
        if isinstance(objAnswer, RTCSessionDescription):
            await pc.setRemoteDescription(objAnswer)
            await waitRemoteDescription(pc)


time_start = None


def current_stamp(): # Resumes the timelapse between the sending of the ping and the reception of the pong
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc): # Creates a WebRTC offer session in order to establish a connection peer-to-peer
    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_pings():
        counter = 0
        while True:
            if counter == 3:
                print("Closing connection... Channel closed")
                channel.close()
                break
            counter += 1
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

        global messageClient
        messageClient = {"type": "bye"}
        byeMessage = json.dumps(messageClient)
        echoConnectionClient.transport.sendto(byeMessage.encode())
        echoConnectionClient.transport.close()
        compare = {"type": "bye"}




    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())
        print(f"Communication channel opened -> {channel.label}")

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    await pc.setLocalDescription(await pc.createOffer())
    await consume_signaling(pc)


if __name__ == '__main__':
    pc = RTCPeerConnection()
    coro = run_offer(pc)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
